using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("SR.WebRTC.RuntimeTests")]
[assembly: InternalsVisibleTo("SR.WebRTC.EditorTests")]
