using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Threading.Tasks;
//using Newtonsoft.Json;
using WatsonWebserver;
using WatsonWebserver.Extensions.HostBuilderExtension;
using WatsonWebserver.Core;
//using Unity.Plastic.Newtonsoft.Json;
using Newtonsoft.Json;
using System.Linq;

public class SRSignalingServer : MonoBehaviour
{
    public class Offer
    {
        public string sdp { get; set; }
        public long datetime { get; set; }

        public Offer(string sdp, long datetime)
        {
            this.sdp = sdp;
            this.datetime = datetime;
        }
    }

    public class Answer
    {
        public string sdp { get; set; }
        public long datetime { get; set; }

        public Answer(string sdp, long datetime)
        {
            this.sdp = sdp;
            this.datetime = datetime;
        }
    }

    public class Candidate
    {
        public string candidate { get; set; }
        public int sdpMLineIndex { get; set; }
        public string sdpMid { get; set; }
        public long datetime { get; set; }

        public Candidate(string candidate, int sdpMLineIndex, string sdpMid, long datetime)
        {
            this.candidate = candidate;
            this.sdpMLineIndex = sdpMLineIndex;
            this.sdpMid = sdpMid;
            this.datetime = datetime;
        }
    }

    public class DeleteConnectionRequest
    {
        public string ConnectionId { get; set; }
    }

    public class PostOfferRequest
    {
        public string ConnectionId { get; set; }
        public string Sdp { get; set; }
    }

    public class PostAnswerRequest
    {
        public string ConnectionId { get; set; }
        public string Sdp { get; set; }
    }

    public class PostCandidateRequest
    {
        public string ConnectionId { get; set; }
        public string Candidate { get; set; }
        public int SdpMLineIndex { get; set; }
        public string SdpMid { get; set; }
    }

    private static Dictionary<string, HashSet<string>> clients = new Dictionary<string, HashSet<string>>();
    private static Dictionary<string, (string, string)> connectionPair = new Dictionary<string, (string, string)>();
    private static Dictionary<string, Offer> offers = new Dictionary<string, Offer>();
    private static Dictionary<string, Answer> answers = new Dictionary<string, Answer>();
    private static Dictionary<string, Dictionary<string, List<Candidate>>> candidates = new Dictionary<string, Dictionary<string, List<Candidate>>>();

    private static async Task DefaultRoute(HttpContextBase ctx) => await ctx.Response.Send("Hello from default route!");

    private static async Task ProtocolRoute(HttpContextBase ctx)
    {
        var config = new { websocket = true };
        await ctx.Response.Send(JsonConvert.SerializeObject(config));
    }

    private static async Task GetOffersRoute(HttpContextBase ctx)
    {
        var fromTime = ctx.Request.Query.Elements["fromtime"];
        long fromTimeValue = 0;
        if (!string.IsNullOrEmpty(fromTime))
        {
            fromTimeValue = Convert.ToInt64(fromTime);
        }

        var arrayOffers = offers;
        if (fromTimeValue > 0)
        {
            arrayOffers = offers.Where(v => v.Value.datetime > fromTimeValue).ToDictionary(v => v.Key, v => v.Value);
        }
        var obj = arrayOffers.Select(v => new { connectionId = v.Key, sdp = v.Value.sdp }).ToList();
        await ctx.Response.Send(JsonConvert.SerializeObject(new { offers = obj }));
    }

    private static async Task GetAnswersRoute(HttpContextBase ctx)
    {
        var fromTime = ctx.Request.Query.Elements["fromtime"];
        long fromTimeValue = 0;
        if (!string.IsNullOrEmpty(fromTime))
        {
            fromTimeValue = Convert.ToInt64(fromTime);
        }

        var sessionId = ctx.Request.Headers["session-id"];
        if (!clients.ContainsKey(sessionId))
        {
            ctx.Response.StatusCode = 404;
            return;
        }

        var connectionIds = clients[sessionId].Where(v => answers.ContainsKey(v)).ToList();
        var arr = new List<object>();
        foreach (var connectionId in connectionIds)
        {
            var answer = answers[connectionId];
            if (answer.datetime > fromTimeValue)
            {
                arr.Add(new { connectionId, sdp = answer.sdp });
            }
        }
        await ctx.Response.Send(JsonConvert.SerializeObject(new { answers = arr }));
    }

    private static async Task GetCandidatesRoute(HttpContextBase ctx)
    {
        var fromTime = ctx.Request.Query.Elements["fromtime"];
        long fromTimeValue = 0;
        if (!string.IsNullOrEmpty(fromTime))
        {
            fromTimeValue = Convert.ToInt64(fromTime);
        }

        var sessionId = ctx.Request.Headers["session-id"];
        if (!clients.ContainsKey(sessionId))
        {
            ctx.Response.StatusCode = 404;
            return;
        }

        var connectionIds = clients[sessionId];
        var arr = new List<object>();
        foreach (var connectionId in connectionIds)
        {
            if (!connectionPair.ContainsKey(connectionId)) continue;

            var pair = connectionPair[connectionId];
            var otherSessionId = sessionId == pair.Item1 ? pair.Item2 : pair.Item1;

            if (!candidates.ContainsKey(otherSessionId) || !candidates[otherSessionId].ContainsKey(connectionId))
            {
                continue;
            }

            var arrayCandidates = candidates[otherSessionId][connectionId]
                .Where(v => v.datetime > fromTimeValue)
                .Select(v => new { candidate = v.candidate, sdpMLineIndex = v.sdpMLineIndex, sdpMid = v.sdpMid })
                .ToList();

            if (arrayCandidates.Count == 0) continue;

            arr.Add(new { connectionId, candidates = arrayCandidates });
        }
        await ctx.Response.Send(JsonConvert.SerializeObject(new { candidates = arr }));
    }

    private static async Task CreateSessionRoute(HttpContextBase ctx)
    {
        var id = Guid.NewGuid().ToString();
        clients[id] = new HashSet<string>();
        await ctx.Response.Send(JsonConvert.SerializeObject(new { sessionId = id }));
    }

    private static async Task DeleteSessionRoute(HttpContextBase ctx)
    {
        var sessionId = ctx.Request.Headers["session-id"];
        if (clients.ContainsKey(sessionId))
        {
            clients.Remove(sessionId);
        }
        ctx.Response.StatusCode = 200;
    }

    private static async Task CreateConnectionRoute(HttpContextBase ctx)
    {
        var sessionId = ctx.Request.Headers["session-id"];
        var connectionId = Guid.NewGuid().ToString();
        if (!clients.ContainsKey(sessionId))
        {
            clients[sessionId] = new HashSet<string>();
        }
        clients[sessionId].Add(connectionId);
        await ctx.Response.Send(JsonConvert.SerializeObject(new { connectionId }));
    }

    private static async Task DeleteConnectionRoute(HttpContextBase ctx)
    {
        var sessionId = ctx.Request.Headers["session-id"];
        var body = await ReadRequestBodyAsync(ctx.Request);
        var connectionId = JsonConvert.DeserializeObject<DeleteConnectionRequest>(body).ConnectionId;

        if (clients.ContainsKey(sessionId))
        {
            clients[sessionId].Remove(connectionId);
        }
        if (connectionPair.ContainsKey(connectionId))
        {
            connectionPair.Remove(connectionId);
        }
        ctx.Response.StatusCode = 200;
    }

    private static async Task PostOfferRoute(HttpContextBase ctx)
    {
        var sessionId = ctx.Request.Headers["session-id"];
        var body = await ReadRequestBodyAsync(ctx.Request);
        var request = JsonConvert.DeserializeObject<PostOfferRequest>(body);

        offers[request.ConnectionId] = new Offer(request.Sdp, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds());
        connectionPair[request.ConnectionId] = (sessionId, null);
        ctx.Response.StatusCode = 200;
    }

    private static async Task PostAnswerRoute(HttpContextBase ctx)
    {
        var sessionId = ctx.Request.Headers["session-id"];
        var body = await ReadRequestBodyAsync(ctx.Request);
        Debug.Log("SignalingDebug: Next: DeserializeObject body.");
        var request = JsonConvert.DeserializeObject<PostAnswerRequest>(body);

        if (!clients.ContainsKey(sessionId))
        {
            clients[sessionId] = new HashSet<string>();
        }
        clients[sessionId].Add(request.ConnectionId);
        answers[request.ConnectionId] = new Answer(request.Sdp, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds());

        if (connectionPair.ContainsKey(request.ConnectionId))
        {
            var pair = connectionPair[request.ConnectionId];
            var otherSessionId = pair.Item1;
            connectionPair[request.ConnectionId] = (otherSessionId, sessionId);

            if (candidates.ContainsKey(otherSessionId))
            {
                if (candidates[otherSessionId].ContainsKey(request.ConnectionId))
                {
                    foreach (var candidate in candidates[otherSessionId][request.ConnectionId])
                    {
                        candidate.datetime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                    }
                }
            }
        }
        ctx.Response.StatusCode = 200;
    }

    private static async Task PostCandidateRoute(HttpContextBase ctx)
    {
        var sessionId = ctx.Request.Headers["session-id"];
        var body = await ReadRequestBodyAsync(ctx.Request);
        var request = JsonConvert.DeserializeObject<PostCandidateRequest>(body);

        if (!candidates.ContainsKey(sessionId))
        {
            candidates[sessionId] = new Dictionary<string, List<Candidate>>();
        }
        if (!candidates[sessionId].ContainsKey(request.ConnectionId))
        {
            candidates[sessionId][request.ConnectionId] = new List<Candidate>();
        }
        var candidate = new Candidate(request.Candidate, request.SdpMLineIndex, request.SdpMid, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds());
        candidates[sessionId][request.ConnectionId].Add(candidate);
        ctx.Response.StatusCode = 200;
    }

    private static async Task<string> ReadRequestBodyAsync(HttpRequestBase req)
    {
        using (var reader = new StreamReader(req.Data))
        {
            return await reader.ReadToEndAsync();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        var server = new HostBuilder("127.0.0.1", 8000, true, DefaultRoute)
            .MapStaticRoute(HttpMethod.GET, "/protocol", ProtocolRoute)
            .MapStaticRoute(HttpMethod.GET, "/signaling/offer", GetOffersRoute)
            .MapStaticRoute(HttpMethod.GET, "/signaling/answer", GetAnswersRoute)
            .MapStaticRoute(HttpMethod.GET, "/signaling/candidate", GetCandidatesRoute)
            .MapStaticRoute(HttpMethod.PUT, "/signaling", CreateSessionRoute)
            .MapStaticRoute(HttpMethod.DELETE, "/signaling", DeleteSessionRoute)
            .MapStaticRoute(HttpMethod.PUT, "/signaling/connection", CreateConnectionRoute)
            .MapStaticRoute(HttpMethod.DELETE, "/signaling/connection",  DeleteConnectionRoute)
            .MapStaticRoute(HttpMethod.POST, "/signaling/offer", PostOfferRoute)
            .MapStaticRoute(HttpMethod.POST, "/signaling/answer", PostAnswerRoute)
            .MapStaticRoute(HttpMethod.POST, "/signaling/candidate", PostCandidateRoute)
            .Build();

        server.Start();
        Debug.Log("Server started");
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
