using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace SR.WebRTC
{
    public class MediaStreamTrack : IDisposable
    {
        public IntPtr self;
        protected bool disposed;
        private bool enabled;
        private TrackState readyState;
        internal Action<MediaStreamTrack> stopTrack;
        protected int frameRateDownScale;
        protected int frameCounter;
        protected int mdataCounter;

        /// <summary>
        ///
        /// </summary>
        public bool Enabled
        {
            get
            {
                return NativeMethods.MediaStreamTrackGetEnabled(self);
            }
            set
            {
                NativeMethods.MediaStreamTrackSetEnabled(self, value);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public TrackState ReadyState
        {
            get
            {
                return NativeMethods.MediaStreamTrackGetReadyState(self);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public TrackKind Kind { get; }

        /// <summary>
        ///
        /// </summary>
        public string Id { get; }

        internal MediaStreamTrack(IntPtr ptr)
        {
            self = ptr;
            WebRTC.Table.Add(self, this);
            Kind = NativeMethods.MediaStreamTrackGetKind(self);
            Id = NativeMethods.MediaStreamTrackGetID(self).AsAnsiStringWithFreeMem();
        }

        ~MediaStreamTrack()
        {
            this.Dispose();
            WebRTC.Table.Remove(self);
        }

        public virtual void Dispose()
        {
            if (this.disposed)
            {
                return;
            }
            if (self != IntPtr.Zero && !WebRTC.Context.IsNull)
            {
                WebRTC.Context.DeleteMediaStreamTrack(self);
                self = IntPtr.Zero;
            }
            this.disposed = true;
            GC.SuppressFinalize(this);
        }

        //Disassociate track from its source(video or audio), not for destroying the track
        public void Stop()
        {
            stopTrack(this);
        }
    }

    public class VideoStreamTrack : MediaStreamTrack
    {
        internal static List<VideoStreamTrack> tracks = new List<VideoStreamTrack>();

        readonly bool m_needFlip = false;
        UnityEngine.Texture m_sourceTexture;
        //readonly UnityEngine.RenderTexture m_destTexture;

        static int buffersize = 10;
        float[] curSfps = new float[buffersize];
        float[][] curRotations = new float[buffersize][];
        float[][] curPositions = new float[buffersize][];
        int[] clientTimestamps = new int[buffersize];
        int[] serverTimestamps = new int[buffersize];
        bool curStereo = true;
        public bool encodeNext = false;

        [StructLayout(LayoutKind.Sequential)]
        public struct FrameMetadata
        {
            public IntPtr track;
            public float sfp;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public float[] rotation;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public float[] positions;
            public int clientTimestamp;
            public int serverTimestamp;
            [MarshalAs(UnmanagedType.I1)]
            public bool stereo;
        }
        public static FrameMetadata frameMetaData = new();
        public IntPtr metaDataPtr = Marshal.AllocHGlobal(Marshal.SizeOf(frameMetaData));

        private static UnityEngine.RenderTexture CreateRenderTexture(int width, int height, UnityEngine.RenderTextureFormat format)
        {
            //var tex = new UnityEngine.RenderTexture(width, height, 0, format);
            var tex = new UnityEngine.RenderTexture(width, height, 0, format, RenderTextureReadWrite.sRGB);
            tex.Create();
            return tex;
        }

        internal VideoStreamTrack(string label, UnityEngine.RenderTexture source, int width, int height)
            : this(label, source.GetNativeTexturePtr(), width, height)
        {
            m_needFlip = true;
            m_sourceTexture = source;
            //m_destTexture = dest;
        }

        //internal VideoStreamTrack(string label, UnityEngine.Texture source, UnityEngine.RenderTexture dest, int width, int height)
        //    : this(label, dest.GetNativeTexturePtr(), width, height)
        //{
        //    m_needFlip = true;
        //    m_sourceTexture = source;
        //    m_destTexture = dest;
        //}

        internal VideoStreamTrack(string label, UnityEngine.RenderTexture source, int width, int height, bool YUVDepth, int depthEncoding, bool secondView, bool losslessDepth, bool enableAsyncEncoding, bool parallel, bool dumpEncodedVideoToFile, bool verboseLogs, int depthBits, int depthPackPeriod, int bitrateKbps, int fps)
            : this(label, source.GetNativeTexturePtr(), width, height, YUVDepth, depthEncoding, secondView, losslessDepth, enableAsyncEncoding, dumpEncodedVideoToFile, verboseLogs, depthBits, depthPackPeriod, bitrateKbps, fps)
        {
            if (parallel && YUVDepth)
            {
                m_needFlip = false;
            }
            else
            {
                m_needFlip = true;
            }
            m_sourceTexture = source;
            //m_destTexture = dest;
            frameRateDownScale = 1;
            frameCounter = 0;
            mdataCounter = 0;
        }

        //internal VideoStreamTrack(string label, UnityEngine.Texture source, UnityEngine.RenderTexture dest, int width, int height, bool YUVDepth, int depthEncoding, bool secondView, bool losslessDepth, bool enableAsyncEncoding, bool parallel, bool dumpEncodedVideoToFile, bool verboseLogs, int depthBits, int depthPackPeriod, int bitrateKbps, int fps)
        //    : this(label, dest.GetNativeTexturePtr(), width, height, YUVDepth, depthEncoding, secondView, losslessDepth, enableAsyncEncoding, dumpEncodedVideoToFile, verboseLogs, depthBits, depthPackPeriod, bitrateKbps, fps)
        //{
        //    if (parallel && YUVDepth)
        //    {
        //        m_needFlip = false;
        //    }
        //    else
        //    {
        //        m_needFlip = true;
        //    }
        //    m_sourceTexture = source;
        //    m_destTexture = dest;
        //    frameRateDownScale = 1;
        //    frameCounter = 0;
        //    mdataCounter = 0;
        //}

        /// <summary>
        /// note:
        /// The videotrack cannot be used if the encoder has not been initialized.
        /// Do not use it until the initialization is complete.
        /// </summary>
        public bool IsInitialized
        {
            get
            {
                return WebRTC.Context.GetInitializationResult(self) == CodecInitializationResult.Success;
            }
        }

        //internal void Update()
        //{
        //    if (encodeNext)
        //    {
        //        if(frameCounter == 0)
        //        {
        //            frameCounter = mdataCounter;
        //        } else
        //        {
        //            frameCounter++;
        //        }
        //        //frameCounter = 0;
        //        // [Note-kazuki: 2020-03-09] Flip vertically RenderTexture
        //        // note: streamed video is flipped vertical if no action was taken:
        //        //  - duplicate RenderTexture from its source texture
        //        //  - call Graphics.Blit command with flip material every frame
        //        //  - it might be better to implement this if possible
        //        if (m_needFlip)
        //        {
        //            UnityEngine.Graphics.Blit(m_sourceTexture, m_destTexture, WebRTC.flipMat);
        //        }
        //        else
        //        {
        //            UnityEngine.Graphics.Blit(m_sourceTexture, m_destTexture);
        //        }
        //        //SR metadata
        //        //UnityEngine.Debug.Log(DateTime.UtcNow.ToString("hh.mm.ss.ffffff") + " SRdebug: WebRTC setting metadata, frameCounter: " + frameCounter);
        //        WebRTC.Context.SetFrameMetadata(self, curSfps[frameCounter % buffersize], curRotations[frameCounter % buffersize], curPositions[frameCounter % buffersize], clientTimestamps[frameCounter % buffersize], serverTimestamps[frameCounter % buffersize], curStereo);
        //        WebRTC.Context.Encode(self);

        //        encodeNext = false;
        //    }
        //}

        public void EncodeTrack()
        {
            if (encodeNext && WebRTC.startEncoding)
            {
                /*
                if (frameCounter == 0)
                {
                    frameCounter = mdataCounter;
                }
                else
                {
                    frameCounter++;
                }
                */
                //SR metadata
                //UnityEngine.Debug.Log(DateTime.UtcNow.ToString("hh.mm.ss.ffffff") + " SRdebug: WebRTC setting metadata, frameCounter: " + frameCounter);
                //WebRTC.Context.SetFrameMetadata(self, curSfps[frameCounter % buffersize], curRotations[frameCounter % buffersize], curPositions[frameCounter % buffersize], clientTimestamps[frameCounter % buffersize], serverTimestamps[frameCounter % buffersize], curStereo);
                //Debug.Log(frameCounter + ", SyncDebug: EncodeTrack rotation.x:" + curRotations[frameCounter % buffersize][0]);
                //WebRTC.Context.SetFrameMetadataRenderThread(self, curSfps[frameCounter % buffersize], curRotations[frameCounter % buffersize], curPositions[frameCounter % buffersize], clientTimestamps[frameCounter % buffersize], serverTimestamps[frameCounter % buffersize], curStereo);
                //Debug.Log("SyncDebug: calling Context.Encode()");
                WebRTC.Context.Encode(self);

                encodeNext = false;
            }
        }

        public void SetFrameMetaData()
        {
            if (encodeNext && WebRTC.startEncoding)
            {
                if (frameCounter == 0)
                {
                    frameCounter = mdataCounter;
                }
                else
                {
                    frameCounter++;
                }
                //SR metadata
                //UnityEngine.Debug.Log(DateTime.UtcNow.ToString("hh.mm.ss.ffffff") + " SRdebug: WebRTC setting metadata, frameCounter: " + frameCounter);
                WebRTC.Context.SetFrameMetadata(self, curSfps[frameCounter % buffersize], curRotations[frameCounter % buffersize], curPositions[frameCounter % buffersize], clientTimestamps[frameCounter % buffersize], serverTimestamps[frameCounter % buffersize], curStereo);
                //Debug.Log(frameCounter + ", SyncDebug: EncodeTrack rotation.x:" + curRotations[frameCounter % buffersize][0]);
                //Debug.Log("Metadata debug: SetFrameMetaData() called:" + self.ToString());
                //WebRTC.Context.SetFrameMetadataRenderThread(self, curSfps[frameCounter % buffersize], curRotations[frameCounter % buffersize], curPositions[frameCounter % buffersize], clientTimestamps[frameCounter % buffersize], serverTimestamps[frameCounter % buffersize], curStereo);
                //Debug.Log("SyncDebug: calling Context.Encode()");
                //WebRTC.Context.Encode(self);

                //encodeNext = false;
            }
        }


        public void UpdateSourceTexture(UnityEngine.Texture source)
        {
            m_sourceTexture = source;
        }

        public void UpdateFrameMetadata(float sfp, float[] rotation, float[] positions, int clientTimestamp, int serverTimestamp, bool stereo)
        {
            mdataCounter++;
            //Debug.Log(mdataCounter + ", SyncDebug: UpdateFrameMetaData rotation.x:" + rotation[0]);
            //mdataCounter = 0;
            curSfps[mdataCounter % buffersize] = sfp;
            curRotations[mdataCounter % buffersize] = rotation;
            curPositions[mdataCounter % buffersize] = positions;
            clientTimestamps[mdataCounter % buffersize] = clientTimestamp;
            serverTimestamps[mdataCounter % buffersize] = serverTimestamp;
            curStereo = stereo;
            /*
            frameMetaData = new FrameMetadata
            {
                track = self,
                sfp = sfp,
                rotation = rotation,
                positions = positions,
                clientTimestamp = clientTimestamp,
                serverTimestamp = serverTimestamp,
                stereo = stereo
            };
            metaDataPtr = Marshal.AllocHGlobal(Marshal.SizeOf(frameMetaData));
            Marshal.StructureToPtr(frameMetaData, metaDataPtr, true);
            */
            //UnityEngine.Debug.Log(DateTime.UtcNow.ToString("hh.mm.ss.ffffff") + " SRdebug: WebRTC updated metadata, mdataCounter: " + mdataCounter);
        }

        public void EncodeNextFrame()
        {
            encodeNext = true;
        }

        public int GetFrameEncodeDelay()
        {
            return WebRTC.Context.GetFrameEncodeDelay(self);
        }

        //public void SetFrameRateScale(int fRateScale)
        //{
        //    frameRateDownScale = fRateScale;
        //}

        /// <summary>
        /// Creates a new VideoStream object.
        /// The track is created with a `source`.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="source"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public VideoStreamTrack(string label, UnityEngine.RenderTexture source)
            : this(label, source, source.width, source.height)
        {
        }

        //public VideoStreamTrack(string label, UnityEngine.RenderTexture source)
        //    : this(label, source, CreateRenderTexture(source.width, source.height, source.format), source.width, source.height)
        //{
        //}

        public VideoStreamTrack(string label, UnityEngine.RenderTexture source, bool YUVDepth, int depthEncoding, bool secondView, bool losslessDepth, bool enableAsyncEncoding, bool parallel, bool dumpEncodedVideoToFile, bool verboseLogs, int depthBits, int depthPackPeriod, int bitrateKbps, int fps)
            : this(label, source, source.width, source.height, YUVDepth, depthEncoding, secondView, losslessDepth, enableAsyncEncoding, parallel, dumpEncodedVideoToFile, verboseLogs, depthBits, depthPackPeriod, bitrateKbps, fps)
        {
        }

        //public VideoStreamTrack(string label, UnityEngine.RenderTexture source, bool YUVDepth, int depthEncoding, bool secondView, bool losslessDepth, bool enableAsyncEncoding, bool parallel, bool dumpEncodedVideoToFile, bool verboseLogs, int depthBits, int depthPackPeriod, int bitrateKbps, int fps)
        //    : this(label, source, CreateRenderTexture(source.width, source.height, source.format), source.width, source.height, YUVDepth, depthEncoding, secondView, losslessDepth, enableAsyncEncoding, parallel, dumpEncodedVideoToFile, verboseLogs, depthBits, depthPackPeriod, bitrateKbps, fps)
        //{
        //}

        //public VideoStreamTrack(string label, UnityEngine.Texture source)
        //    : this(label,
        //        source,
        //        CreateRenderTexture(source.width, source.height, WebRTC.GetSupportedRenderTextureFormat(SystemInfo.graphicsDeviceType)),
        //        source.width,
        //        source.height)
        //{
        //}


        /// <summary>
        /// Creates a new VideoStream object.
        /// The track is created with a source texture `ptr`.
        /// It is noted that streamed video might be flipped when not action was taken. Almost case it has no problem to use other constructor instead.
        ///
        /// See Also: Texture.GetNativeTexturePtr
        /// </summary>
        /// <param name="label"></param>
        /// <param name="ptr"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public VideoStreamTrack(string label, IntPtr ptr, int width, int height)
            : base(WebRTC.Context.CreateVideoTrack(label, ptr))
        {
            WebRTC.Context.SetVideoEncoderParameter(self, width, height);
            WebRTC.Context.InitializeEncoder(self);
            tracks.Add(this);
        }

        public VideoStreamTrack(string label, IntPtr ptr, int width, int height, bool YUVDepth, int depthEncoding, bool secondView, bool losslessDepth, bool enableAsyncEncoding, bool dumpEncodedVideoToFile, bool verboseLogs, int depthBits, int depthPackPeriod, int bitrateKbps, int fps)
            : base(WebRTC.Context.CreateVideoTrack(label, ptr))
        {
            WebRTC.Context.SetVideoEncoderParameter(self, width, height, YUVDepth, depthEncoding, secondView, losslessDepth, enableAsyncEncoding, dumpEncodedVideoToFile, verboseLogs, depthBits, depthPackPeriod, bitrateKbps, fps);
            WebRTC.Context.InitializeEncoder(self);
            tracks.Add(this);
        }

        
        public override void Dispose()
        {
            if (this.disposed)
            {
                return;
            }
            if (self != IntPtr.Zero && !WebRTC.Context.IsNull)
            {
                WebRTC.Context.FinalizeEncoder(self);
                tracks.Remove(this);
                WebRTC.Context.DeleteMediaStreamTrack(self);
                //UnityEngine.Object.DestroyImmediate(m_destTexture);
                self = IntPtr.Zero;
            }
            this.disposed = true;
            GC.SuppressFinalize(this);
        }
    }

    public class AudioStreamTrack : MediaStreamTrack
    {
        public AudioStreamTrack(string label) : base(WebRTC.Context.CreateAudioTrack(label))
        {
        }
    }

    public enum TrackKind
    {
        Audio,
        Video
    }
    public enum TrackState
    {
        Live,
        Ended
    }

    public class RTCTrackEvent
    {
        public MediaStreamTrack Track { get; }

        public RTCRtpTransceiver Transceiver { get; }

        public RTCRtpReceiver Receiver
        {
            get
            {
                return Transceiver.Receiver;
            }
        }

        internal RTCTrackEvent(IntPtr ptrTransceiver, RTCPeerConnection peer)
        {
            IntPtr ptrTrack = NativeMethods.TransceiverGetTrack(ptrTransceiver);
            Track = WebRTC.FindOrCreate(ptrTrack, ptr => new MediaStreamTrack(ptr));
            Transceiver = new RTCRtpTransceiver(ptrTransceiver, peer);
        }
    }

    public class MediaStreamTrackEvent
    {
        public MediaStreamTrack Track { get; }

        internal MediaStreamTrackEvent(IntPtr ptrTrack)
        {
            Track = WebRTC.FindOrCreate(ptrTrack, ptr => new MediaStreamTrack(ptr));
        }
    }
}

