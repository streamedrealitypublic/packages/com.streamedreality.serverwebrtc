namespace SR.WebRTC
{
    public enum RenderTextureDepth {
        DEPTH_16 = 16,
        DEPTH_24 = 24,
        DEPTH_32 = 32,
    }
}
