using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Threading.Tasks;
using WatsonWebserver.Lite;
using WatsonWebserver.Core;
using Newtonsoft.Json;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Operators;
using Org.BouncyCastle.Crypto.Parameters;
using System.Globalization;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using System.IO;

public class SRSignalingServerLite : MonoBehaviour
{
    private int signalingServerPort = 443;
    private bool launchSignalingServer = true;
    public class Offer
    {
        public string sdp { get; set; }
        public long datetime { get; set; }

        public Offer(string sdp, long datetime)
        {
            this.sdp = sdp;
            this.datetime = datetime;
        }
    }

    public class Answer
    {
        public string sdp { get; set; }
        public long datetime { get; set; }

        public Answer(string sdp, long datetime)
        {
            this.sdp = sdp;
            this.datetime = datetime;
        }
    }

    public class Candidate
    {
        public string candidate { get; set; }
        public int sdpMLineIndex { get; set; }
        public string sdpMid { get; set; }
        public long datetime { get; set; }

        public Candidate(string candidate, int sdpMLineIndex, string sdpMid, long datetime)
        {
            this.candidate = candidate;
            this.sdpMLineIndex = sdpMLineIndex;
            this.sdpMid = sdpMid;
            this.datetime = datetime;
        }
    }

    public class DeleteConnectionRequest
    {
        public string ConnectionId { get; set; }
    }

    public class PostOfferRequest
    {
        public string ConnectionId { get; set; }
        public string Sdp { get; set; }
    }

    public class PostAnswerRequest
    {
        public string ConnectionId { get; set; }
        public string Sdp { get; set; }
    }

    public class PostCandidateRequest
    {
        public string ConnectionId { get; set; }
        public string Candidate { get; set; }
        public int SdpMLineIndex { get; set; }
        public string SdpMid { get; set; }
    }

    private static ConcurrentDictionary<string, HashSet<string>> clients = new ConcurrentDictionary<string, HashSet<string>>();
    private static ConcurrentDictionary<string, (string, string)> connectionPair = new ConcurrentDictionary<string, (string, string)>();
    private static ConcurrentDictionary<string, Offer> offers = new ConcurrentDictionary<string, Offer>();
    private static ConcurrentDictionary<string, Answer> answers = new ConcurrentDictionary<string, Answer>();
    private static ConcurrentDictionary<string, ConcurrentDictionary<string, List<Candidate>>> candidates = new ConcurrentDictionary<string, ConcurrentDictionary<string, List<Candidate>>>();

    private static async Task DefaultRoute(HttpContextBase ctx) => await ctx.Response.Send("Hello from default route!");
    private static async Task ProtocolRoute(HttpContextBase ctx)
    {
        var config = new { websocket = true };
        await ctx.Response.Send(JsonConvert.SerializeObject(config));
    }

    private static async Task GetOffersRoute(HttpContextBase ctx)
    {
        //Debug.Log("SRSignalingServer: GetOffersRoute task starting.");
        var fromTime = ctx.Request.Query.Elements["fromtime"];
        long fromTimeValue = 0;
        if (!string.IsNullOrEmpty(fromTime))
        {
            fromTimeValue = Convert.ToInt64(fromTime);
        }
        //Debug.Log($"SRSignalingServer: fromTimeValue: {fromTimeValue}");

        var arrayOffers = offers;
        if (fromTimeValue > 0)
        {
            arrayOffers = new ConcurrentDictionary<string, Offer>(offers.Where(v => v.Value.datetime > fromTimeValue));
        }
        //Debug.Log($"SRSignalingServer: arrayOffers count: {arrayOffers.Count}");

        var obj = arrayOffers.Select(v => new { connectionId = v.Key, sdp = v.Value.sdp }).ToList();
        await ctx.Response.Send(JsonConvert.SerializeObject(new { offers = obj }));
        //Debug.Log("SRSignalingServer: GetOffersRoute task ending.");
    }

    private static async Task GetAnswersRoute(HttpContextBase ctx)
    {
        var fromTime = ctx.Request.Query.Elements["fromtime"];
        long fromTimeValue = 0;
        if (!string.IsNullOrEmpty(fromTime))
        {
            fromTimeValue = Convert.ToInt64(fromTime);
        }

        var sessionId = ctx.Request.Headers["session-id"];
        if (!clients.ContainsKey(sessionId))
        {
            ctx.Response.StatusCode = 404;
            return;
        }

        var connectionIds = clients[sessionId].Where(v => answers.ContainsKey(v)).ToList();
        var arr = new List<object>();
        foreach (var connectionId in connectionIds)
        {
            var answer = answers[connectionId];
            if (answer.datetime > fromTimeValue)
            {
                arr.Add(new { connectionId, sdp = answer.sdp });
            }
        }
        await ctx.Response.Send(JsonConvert.SerializeObject(new { answers = arr }));
    }

    private static async Task GetCandidatesRoute(HttpContextBase ctx)
    {
        //Debug.Log("SRSignalingServer: GetCandidatesRoute task starting.");
        //Debug.LogError("SRSignalingServer: GetCandidates length, candidate count: " + candidates.Count);

        var sessionId = ctx.Request.Headers["session-id"];
        if (!clients.ContainsKey(sessionId))
        {
            ctx.Response.StatusCode = 404;
            Debug.Log($"SRSignalingServer: Session ID {sessionId} not found.");
            return;
        }

        var connectionIds = clients[sessionId];
        Debug.Log($"SRSignalingServer: Found {connectionIds.Count} connection IDs for session {sessionId}.");
        var arr = new List<object>();

        foreach (var connectionId in connectionIds)
        {
            if (!connectionPair.ContainsKey(connectionId))
            {
                Debug.Log($"SRSignalingServer: Connection ID {connectionId} not found in connectionPair.");
                continue;
            }

            var pair = connectionPair[connectionId];
            var otherSessionId = sessionId == pair.Item1 ? pair.Item2 : pair.Item1;
            //Debug.Log($"SRSignalingServer: Processing connectionId {connectionId} with otherSessionId {otherSessionId}.");

            if (!candidates.ContainsKey(otherSessionId) || !candidates[otherSessionId].ContainsKey(connectionId))
            {
                Debug.Log($"SRSignalingServer: No candidates found for otherSessionId {otherSessionId} and connectionId {connectionId}.");
                continue;
            }

            var arrayCandidates = candidates[otherSessionId][connectionId]
                .Select(v => new { candidate = v.candidate, sdpMLineIndex = v.sdpMLineIndex, sdpMid = v.sdpMid })
                .ToList();

            if (arrayCandidates.Count == 0)
            {
                Debug.Log($"SRSignalingServer: No candidates after selection for otherSessionId {otherSessionId} and connectionId {connectionId}.");
                continue;
            }

            arr.Add(new { connectionId, candidates = arrayCandidates });
            //Debug.Log($"SRSignalingServer: Added {arrayCandidates.Count} candidates for connectionId {connectionId}.");
        }

        var responseJson = JsonConvert.SerializeObject(new { candidates = arr });
        //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: CandidateDebug: GetCandidatesRoute Response JSON: {responseJson}");
        await ctx.Response.Send(responseJson);

        //Debug.LogError("SRSignalingServer: GetCandidatesRoute task ending, candidate count: " + arr.Count);
    }



    private static async Task CreateSessionRoute(HttpContextBase ctx)
    {
        //Debug.Log("SRSignalingServer: CreateSessionRoute task starting.");
        var id = Guid.NewGuid().ToString();
        clients[id] = new HashSet<string>();
        await ctx.Response.Send(JsonConvert.SerializeObject(new { sessionId = id }));
        //Debug.Log("SRSignalingServer: CreateSessionRoute task ending.");
    }

    private static async Task DeleteSessionRoute(HttpContextBase ctx)
    {
        //Debug.Log("SRSignalingServer: DeleteSessionRoute task starting.");
        var sessionId = ctx.Request.Headers["session-id"];
        clients.TryRemove(sessionId, out _); // safely removes the session if it exists
        ctx.Response.StatusCode = 200;
        await ctx.Response.Send();
        //Debug.Log("SRSignalingServer: DeleteSessionRoute task ending.");
    }

    private static async Task CreateConnectionRoute(HttpContextBase ctx)
    {
        //Debug.Log("SRSignalingServer: CreateConnectionRoute task starting.");
        var sessionId = ctx.Request.Headers["session-id"];
        var connectionId = Guid.NewGuid().ToString();
        if (!clients.ContainsKey(sessionId))
        {
            clients[sessionId] = new HashSet<string>();
        }
        clients[sessionId].Add(connectionId);
        await ctx.Response.Send(JsonConvert.SerializeObject(new { connectionId }));
        //Debug.Log("SRSignalingServer: CreateConnectionRoute task ending.");
    }
    private static string ReadRequestBody(HttpRequestBase req, long contentLength)
    {
        return req.DataAsString;
    }

    private static async Task DeleteConnectionRoute(HttpContextBase ctx)
    {
        var sessionId = ctx.Request.Headers["session-id"];
        var contentLength = long.Parse(ctx.Request.Headers["Content-Length"]);
        var body = ReadRequestBody(ctx.Request, contentLength);
        var connectionId = JsonConvert.DeserializeObject<DeleteConnectionRequest>(body).ConnectionId;

        if (clients.TryGetValue(sessionId, out var clientConnections))
        {
            clientConnections.Remove(connectionId);
            if (clientConnections.Count == 0)
            {
                clients.TryRemove(sessionId, out _);
            }
        }

        connectionPair.TryRemove(connectionId, out _);

        ctx.Response.StatusCode = 200;
        await ctx.Response.Send();
    }

    private static async Task PostOfferRoute(HttpContextBase ctx)
    {
        //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostOfferRoute task starting.");

        try
        {
            var sessionId = ctx.Request.Headers["session-id"];
            Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: Received session-id: {sessionId}");

            string body;
            try
            {
                var contentLength = long.Parse(ctx.Request.Headers["Content-Length"]);
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostOfferRoute content length:" + contentLength);
                body = ReadRequestBody(ctx.Request, contentLength);
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostOfferRoute Request body read successfully. ");
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostOfferRoute Request body read successfully:" + body);
            }
            catch (Exception ex)
            {
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: Error reading request body: {ex.Message}");
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: Stack Trace: {ex.StackTrace}");
                ctx.Response.StatusCode = 400; // Bad Request
                await ctx.Response.Send("SRSignalingServer: Error reading request body: " + ex.Message);
                return;
            }

            PostOfferRequest request;
            try
            {
                request = JsonConvert.DeserializeObject<PostOfferRequest>(body);
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: Request deserialized successfully: {JsonConvert.SerializeObject(request)}");
            }
            catch (Exception ex)
            {
                Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: Error deserializing request body: {ex.Message}");
                Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: Stack Trace: {ex.StackTrace}");
                ctx.Response.StatusCode = 400; // Bad Request
                await ctx.Response.Send("SRSignalingServer: Error deserializing request body: " + ex.Message);
                return;
            }

            offers[request.ConnectionId] = new Offer(request.Sdp, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds());
            //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: Offer stored successfully for connectionId: {request.ConnectionId}");

            connectionPair[request.ConnectionId] = (sessionId, null);
            //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: Connection pair updated successfully.");

            ctx.Response.StatusCode = 200;
            await ctx.Response.Send("Offer processed successfully.");
            //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostOfferRoute: Response sent successfully.");
        }
        catch (Exception ex)
        {
            Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: Error in PostOfferRoute: {ex.Message}");
            Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: Stack Trace: {ex.StackTrace}");

            ctx.Response.StatusCode = 500; // Internal Server Error
            await ctx.Response.Send("SRSignalingServer: Internal Server Error: " + ex.Message);
        }

        //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostOfferRoute task ends.");
    }

    private static async Task PostAnswerRoute(HttpContextBase ctx)
    {
        //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: task starting.");

        try
        {
            var sessionId = ctx.Request.Headers["session-id"];
            //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Received session-id: {sessionId}");

            // Step 1: Read the request body
            string body;
            try
            {
                var contentLength = long.Parse(ctx.Request.Headers["Content-Length"]);
                body = ReadRequestBody(ctx.Request, contentLength);
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Request body read successfully: {body}");
            }
            catch (Exception ex)
            {
                Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Error reading request body: {ex.Message}");
                Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Stack Trace: {ex.StackTrace}");
                ctx.Response.StatusCode = 400; // Bad Request
                await ctx.Response.Send("SRSignalingServer: PostAnswerRouteDebug: Error reading request body: " + ex.Message);
                return;
            }

            // Step 2: Deserialize the JSON body
            PostAnswerRequest request;
            try
            {
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Attempting to deserialize request body.");
                request = JsonConvert.DeserializeObject<PostAnswerRequest>(body);
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Request deserialized successfully: {JsonConvert.SerializeObject(request)}");
            }
            catch (Exception ex)
            {
                Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Error deserializing request body: {ex.Message}");
                Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Stack Trace: {ex.StackTrace}");
                ctx.Response.StatusCode = 400; // Bad Request
                await ctx.Response.Send("SRSignalingServer: PostAnswerRouteDebug: Error deserializing request body: " + ex.Message);
                return;
            }

            // Step 3: Process the request
            try
            {
                if (!clients.ContainsKey(sessionId))
                {
                    clients[sessionId] = new HashSet<string>();
                    //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Created new client entry for session-id: {sessionId}");
                }
                clients[sessionId].Add(request.ConnectionId);
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Added connectionId to clients for session-id: {sessionId}");

                answers[request.ConnectionId] = new Answer(request.Sdp, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds());
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Stored answer for connectionId: {request.ConnectionId}");

                if (connectionPair.ContainsKey(request.ConnectionId))
                {
                    var pair = connectionPair[request.ConnectionId];
                    var otherSessionId = pair.Item1;
                    connectionPair[request.ConnectionId] = (otherSessionId, sessionId);
                    //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Updated connection pair for connectionId: {request.ConnectionId}");

                    if (candidates.ContainsKey(otherSessionId))
                    {
                        //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Found candidates for otherSessionId: {otherSessionId}");
                        if (candidates[otherSessionId].ContainsKey(request.ConnectionId))
                        {
                            //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Found candidate list for connectionId: {request.ConnectionId}");
                            foreach (var candidate in candidates[otherSessionId][request.ConnectionId])
                            {
                                candidate.datetime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Updated candidate datetime for connectionId: {request.ConnectionId}");
                            }
                        }
                    }
                }

                ctx.Response.StatusCode = 200;
                await ctx.Response.Send();
                //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} PostAnswerRouteDebug: Response sent successfully.");
            }
            catch (Exception ex)
            {
                Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Error processing request: {ex.Message}");
                Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Stack Trace: {ex.StackTrace}");
                ctx.Response.StatusCode = 500; // Internal Server Error
                await ctx.Response.Send("SRSignalingServer: PostAnswerRouteDebug: Internal Server Error: " + ex.Message);
            }
        }
        catch (Exception ex)
        {
            Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Unhandled error in PostAnswerRoute: {ex.Message}");
            Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: Stack Trace: {ex.StackTrace}");
            ctx.Response.StatusCode = 500; // Internal Server Error
            await ctx.Response.Send("Internal Server Error: " + ex.Message);
        }
    //Debug.Log($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostAnswerRouteDebug: task ends.");
    }
    
    private static async Task PostCandidateRoute(HttpContextBase ctx)
    {
        // Print the Content-Length header
        var contentLength = long.Parse(ctx.Request.Headers["Content-Length"]);
        //Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} Content-Length: {contentLength}");

        var sessionId = ctx.Request.Headers["session-id"];
        //Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostDebug ReadRequestBody.");

        var body = ReadRequestBody(ctx.Request, contentLength);

        //Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} CandidateDebug SRSignalingServer: PostDebug DeserializeObject: " + body);
        var request = JsonConvert.DeserializeObject<PostCandidateRequest>(body);

        var sessionCandidates = candidates.GetOrAdd(sessionId, _ => new ConcurrentDictionary<string, List<Candidate>>());
        var connectionCandidates = sessionCandidates.GetOrAdd(request.ConnectionId, _ => new List<Candidate>());

        lock (connectionCandidates)
        {
            var candidate = new Candidate(request.Candidate, request.SdpMLineIndex, request.SdpMid, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds());
            connectionCandidates.Add(candidate);
        }
        
        ctx.Response.StatusCode = 200;
        //Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostDebug PostCandidateRoute: sending response.");
        await ctx.Response.Send("OK");

        //Debug.LogError($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SRSignalingServer: PostDebug PostCandidateRoute: task ending.");
    }
    private static async Task PutSignalingRoute(HttpContextBase ctx)
    {
        var id = Guid.NewGuid().ToString();
        clients[id] = new HashSet<string>();
        await ctx.Response.Send(JsonConvert.SerializeObject(new { sessionId = id }));
    }

    private X509Certificate2 GenerateSelfSignedCertificate(string subjectName)
    {
        // Generate random number
        var random = new SecureRandom(new CryptoApiRandomGenerator());

        // The certificate generator
        var certificateGenerator = new X509V3CertificateGenerator();

        // Serial number
        var serialNumber = BigInteger.ProbablePrime(120, random);
        certificateGenerator.SetSerialNumber(serialNumber);

        // Subject and issuer name
        var subjectDN = new X509Name(subjectName);
        var issuerDN = subjectDN;
        certificateGenerator.SetIssuerDN(issuerDN);
        certificateGenerator.SetSubjectDN(subjectDN);

        // Valid for 1 year
        var notBefore = DateTime.UtcNow.Date;
        var notAfter = notBefore.AddYears(1);
        certificateGenerator.SetNotBefore(notBefore);
        certificateGenerator.SetNotAfter(notAfter);

        // Generate key pair
        var keyGenerationParameters = new KeyGenerationParameters(random, 2048);
        var keyPairGenerator = new RsaKeyPairGenerator();
        keyPairGenerator.Init(keyGenerationParameters);
        var subjectKeyPair = keyPairGenerator.GenerateKeyPair();

        certificateGenerator.SetPublicKey(subjectKeyPair.Public);

        // Self-sign certificate
        var signatureFactory = new Asn1SignatureFactory("SHA256WithRSA", subjectKeyPair.Private, random);
        var certificate = certificateGenerator.Generate(signatureFactory);

        // Convert BouncyCastle certificate to .NET X509Certificate2
        var x509 = new X509Certificate2(DotNetUtilities.ToX509Certificate(certificate));

        // Add private key to X509Certificate2
        RSA rsa = DotNetUtilities.ToRSA(subjectKeyPair.Private as RsaPrivateCrtKeyParameters);
        x509 = x509.CopyWithPrivateKey(rsa);

        return x509;
    }

    // Start is called before the first frame update
    void Start()
    {
        CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
        CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

        LoadConfigFile();

        if (launchSignalingServer)
        {
            // Create a self-signed certificate
            X509Certificate2 certificate = GenerateSelfSignedCertificate("CN=localhost");
            var sslSettings = new WebserverSettings.SslSettings
            {
                Enable = true,
                SslCertificate = certificate
            };

            WebserverSettings _Settings = new WebserverSettings
            {
                Hostname = "*", // Listen on all network interfaces
                Port = signalingServerPort,
                Ssl = sslSettings
            };
            var server = new WebserverLite(_Settings, DefaultRoute);

            server.Routes.PreAuthentication.Static.Add(HttpMethod.GET, "/protocol", ProtocolRoute);
            server.Routes.PreAuthentication.Static.Add(HttpMethod.GET, "/signaling/offer", GetOffersRoute); // Server calls on its self
            server.Routes.PreAuthentication.Static.Add(HttpMethod.GET, "/signaling/answer", GetAnswersRoute); // Client calls 10221 // nocrash
            server.Routes.PreAuthentication.Static.Add(HttpMethod.GET, "/signaling/candidate", GetCandidatesRoute); // Server calls on its self
            server.Routes.PreAuthentication.Static.Add(HttpMethod.PUT, "/signaling", CreateSessionRoute); // Server calls on its self
            server.Routes.PreAuthentication.Static.Add(HttpMethod.DELETE, "/signaling", DeleteSessionRoute);
            server.Routes.PreAuthentication.Static.Add(HttpMethod.PUT, "/signaling/connection", CreateConnectionRoute); // Client calls 9236 // nocrash
            server.Routes.PreAuthentication.Static.Add(HttpMethod.DELETE, "/signaling/connection", DeleteConnectionRoute);
            server.Routes.PreAuthentication.Static.Add(HttpMethod.POST, "/signaling/offer", PostOfferRoute); // Client calls 9388 // crash
            server.Routes.PreAuthentication.Static.Add(HttpMethod.POST, "/signaling/answer", PostAnswerRoute); // Client calls 11238 // nocrash
            server.Routes.PreAuthentication.Static.Add(HttpMethod.POST, "/signaling/candidate", PostCandidateRoute); // Client calls 11056 // nocrash

            server.Start();
            Debug.Log("SignalingServer started in port: " + signalingServerPort);
        } else
        {
            Debug.Log("Not starting SignalingServer. Disabled in config.");
        }

    }

    public void LoadConfigFile()
    {
        Regex regex = new Regex("\"signalingServerPort\":\\s*(\\d+),");
        var path = System.IO.Path.Combine(Application.dataPath, Application.streamingAssetsPath + "/" + "SRconfig.txt");
        string[] json = null;
        if (File.Exists(path))
        {
            json = File.ReadAllLines(path);
        }
        if (json != null)
        {
            for (int i = 0; i < json.Length; i++)
            {
                string line = json[i];
                if (line.Contains("signalingServerPort"))
                {
                    Match match = regex.Match(line);
                    signalingServerPort = int.Parse(match.Groups[1].Value);
                }
                if (line.Contains("launchSignalingServer") && line.Contains("false"))
                {
                    launchSignalingServer = false;
                }
            }
        }
        else
        {
            Debug.LogError("Failed to load config file from " + path);
        }
    }
}
