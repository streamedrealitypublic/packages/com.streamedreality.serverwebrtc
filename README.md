# Streamed Reality XR Server WebRTC Documentation

**Version:** v1.0.0  
**Authors:** Streamed Reality  
**Target:** Windows

## Description
Streamed Reality Server WebRTC enables WebRTC for the Streamed Reality Server SDK, which enables the conversion of existing or new XR Unity applications for cloud or local computer deployment.

## Quick Start Guide
Please see the README.md in Streamed Reality Server SDK.